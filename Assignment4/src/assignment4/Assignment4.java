/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment4;

import University.College;
import University.Department;
import University.Student;
import University.University;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Cuishaowen
 */
public class Assignment4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<University> universities = new ArrayList<University>();
        University u1 = new University("Northeastern University");
        universities.add(u1);
        University u2 = new University("Boston University");
        universities.add(u2);
        University u3 = new University("Massachusetts Institute of Technology");
        universities.add(u3);
        
        SetupAllData.setAll(universities);
        
        
        EmployementRate employmentRate = new EmployementRate(universities);
        
        
        Scanner reader = new Scanner(System.in);  
        
        boolean b = true;
        
        while(b){
            System.out.println("*************************** Degree Performence Report ***************************");
            System.out.println("1. Compare Employment Rate.");
            System.out.println("2. Compare the Average Number of Published Articles of Professor.");
            System.out.println("3. Compare 2015 Average GPA.");
            System.out.println("4. Compare Average Salary between Engineer Students and Business Students.");
            System.out.println("0. Exit.");
            System.out.printf("Please choose a number above: ");
        
            int choice = reader.nextInt(); 
        
            switch(choice)
            {
                case 1: boolean b1 = true;
                        while(b1){
                            System.out.println("*************************** Compare Level ***************************");
                            System.out.println("************************** Employment Rate **************************");
                            System.out.println("1. Between Departments.");
                            System.out.println("2. Between Colleges.");
                            System.out.println("3. Between Universities.");
                            System.out.println("0. Return to the Main Menu.");
                            System.out.printf("Please choose a number above: ");
                            int c1 = reader.nextInt();
                            switch(c1)
                            {
                                case 1: employmentRate.DepartmentEmploymentRate();break;
                                case 2: employmentRate.CollegeEmploymentRate();break;
                                case 3: employmentRate.UniverisyEmploymentRate();break;
                                case 0: b1 = false;break;
                                default: System.out.println("Please input a number from 1 to 4!");System.out.println();System.out.println();
                            }
                        }
                        break;
                case 2: boolean b2 = true;
                        while(b2){
                            System.out.println("*************************** Compare Level ***************************");
                            System.out.println("******* the Average Number of Published Articles of Professor *******");
                            System.out.println("1. Between Departments.");
                            System.out.println("2. Between Colleges.");
                            System.out.println("3. Between Universities.");
                            System.out.println("0. Return to the Main Menu.");
                            System.out.printf("Please choose a number above: ");
                            int c1 = reader.nextInt();
                            switch(c1)
                            {
                                case 1: Article.departmentProfessorArticles(universities);break;
                                case 2: Article.collegeProfessorArticles(universities);break;
                                case 3: Article.universityProfessorArticles(universities);break;
                                case 0: b2 = false;break;
                                default: System.out.println("Please input a number from 1 to 4!");System.out.println();System.out.println();
                            }
                        }
                        break;
                case 3: boolean b3 = true;
                        while(b3){
                            System.out.println("*************************** Compare Level ***************************");
                            System.out.println("************************** 2015 Average GPA *************************");
                            System.out.println("1. Between Departments.");
                            System.out.println("2. Between Colleges.");
                            System.out.println("3. Between Universities.");
                            System.out.println("0. Return to the Main Menu.");
                            System.out.printf("Please choose a number above: ");
                            int c1 = reader.nextInt();
                            switch(c1)
                            {
                                case 1: GPACalculator.departmentAverageGPA(universities);break;
                                case 2: GPACalculator.collegeAverageGPA(universities);break;
                                case 3: GPACalculator.universityAverageGPA(universities);break;
                                case 0: b3 = false;break;
                                default: System.out.println("Please input a number from 1 to 4!");System.out.println();System.out.println();
                            }
                        }
                        break;
                case 4: System.out.println("******Average Salary between Engineer Students and Business Students******");
                        AverageSalary.departmentProfessorArticles(universities);
                        break;
                case 0: b = false;break;
                default: System.out.println("Please input a number from 1 to 4!");System.out.println();System.out.println();
            }
        }
    }
}
