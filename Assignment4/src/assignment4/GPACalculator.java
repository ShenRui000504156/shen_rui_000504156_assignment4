/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment4;

import University.University;
import University.College;
import University.CourseLoad;
import University.Department;
import University.SeatAssignment;
import University.Student;
import java.util.ArrayList;

/**
 *
 * @author Cuishaowen
 */
public class GPACalculator {
    
    public static void departmentAverageGPA(ArrayList<University> u)
    {
        for(University uni : u)
        {
            if(uni.getUniversityName().equals("Northeastern University"))
            {
                for(College c : uni.getCollege())
                {
                    if(c.getCollegeName().equals("Engineering"))
                    {
                        for(Department d : c.getDepartments())
                        {
                            float dAverageGPA = 0;
                            float dSumGPA = 0;
                            int studentNum = 0;
                            
                            for(Student s : d.getDsd().getDepartmentStudentDirectory())
                            {
                                float sAverageGPA = 0;
                                float sSumGPA = 0;
                                int courseNum = 0;
                                if(s.isGraduated() && s.getGraduatedYear()==2015){
                                    for(CourseLoad cl : s.getTranscript().getCourseLoad())
                                    {
                                        for(SeatAssignment sa : cl.getSeatAssignment())
                                        {
                                            sSumGPA = sSumGPA + sa.getGPA();
                                            courseNum++;
                                        }
                                    }
                                }
                                sAverageGPA = sSumGPA / courseNum;
                                dSumGPA = dSumGPA + sAverageGPA;
                                studentNum++;
                            }
                            
                            dAverageGPA = dSumGPA / studentNum;
                            System.out.println(d.getDepartmentName() + " : " + dAverageGPA);
                        }
                    }
                }
            }
        }
    }
    
    public static void collegeAverageGPA(ArrayList<University> u)
    {
        for(University uni : u)
        {
            if(uni.getUniversityName().equals("Northeastern University"))
            {
                for(College c : uni.getCollege())
                {    
                    float cAverageGPA = 0;
                    float cSumGPA = 0;
                    int studentNum = 0;
                            
                    for(Student s : c.getCsd().getCollegeStudentDirectory())
                    {
                        if(s.isGraduated() && s.getGraduatedYear()==2015)
                        {
                            float sAverageGPA = 0;
                            float sSumGPA = 0;
                            int courseNum = 0;
                            for(CourseLoad cl : s.getTranscript().getCourseLoad())
                            {
                                for(SeatAssignment sa : cl.getSeatAssignment())
                                {
                                    sSumGPA = sSumGPA + sa.getGPA();
                                    courseNum++;
                                }
                            }
                            sAverageGPA = sSumGPA / courseNum;
                            cSumGPA = cSumGPA + sAverageGPA;
                            studentNum++;
                        }
                    }
                            
                    cAverageGPA = cSumGPA / studentNum;
                    System.out.println(c.getCollegeName() + " : " + cAverageGPA);
                }
            }
        }
    }
    
    public static void universityAverageGPA(ArrayList<University> u)
    {
        for(University uni : u)
        {
            float uAverageGPA = 0;
            float uSumGPA = 0;
            int studentNum = 0;
                            
            for(Student s : uni.getUsd().getStudents())
            {
                if(s.isGraduated() && s.getGraduatedYear()==2015)
                {
                    float sAverageGPA = 0;
                    float sSumGPA = 0;
                    int courseNum = 0;
                    for(CourseLoad cl : s.getTranscript().getCourseLoad())
                    {
                        for(SeatAssignment sa : cl.getSeatAssignment())
                        {
                            sSumGPA = sSumGPA + sa.getGPA();
                            courseNum++;
                        }
                    }
                    sAverageGPA = sSumGPA / courseNum;
                    uSumGPA = uSumGPA + sAverageGPA;
                    studentNum++;
                }
            }
                            
            uAverageGPA = uSumGPA / studentNum;
            System.out.println(uni.getUniversityName() + " : " + uAverageGPA);            
        }
    }
}
