/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment4;

/**
 *
 * @author fjj1213
 */
import University.College;
import University.Department;
import University.Teacher;
import University.University;
import java.util.ArrayList;


/**
 *
 * @author fjj1213
 */
public class Article {
    public static void departmentProfessorArticles(ArrayList<University> u){
        for (University nu:u){
            if(nu.getUniversityName().equals("Northeastern University")){
                for(College c:nu.getCollege()){
                    if (c.getCollegeName().equals("Engineering")){
                        for(Department d:c.getDepartments()){
                            float averageArticle=0;
                            float sumArticle=0;
                            int teachernumber=0;
                            for(Teacher t: d.getTeacherCatalog().getTeachers()){
                                sumArticle=t.getPublishedArticles()+sumArticle;
                                teachernumber++;
                            }
                            averageArticle=sumArticle/teachernumber;
                            System.out.println(d.getDepartmentName()+" : "+averageArticle);
                        }
                }
              } 
            }
        }
    }
    
    public static void collegeProfessorArticles(ArrayList<University> u){
        for (University nu:u){
            if(nu.getUniversityName().equals("Northeastern University"))
            {
                for(College c:nu.getCollege())
                {
                    float averageArticle=0;
                    float sumArticle=0;
                    int teachernumber=0;
                    for(Department d:c.getDepartments())
                    {
                        for(Teacher t: d.getTeacherCatalog().getTeachers())
                        {
                            sumArticle=t.getPublishedArticles()+sumArticle;
                            teachernumber++;
                        }
                    }
                averageArticle=sumArticle/teachernumber;
                System.out.println(c.getCollegeName()+" : "+averageArticle);
              }
                
            }
                 
        }
    }
    
    public static void universityProfessorArticles(ArrayList<University> u){
        for (University nu:u){
                            float averageArticle=0;
                            float sumArticle=0;
                            int teachernumber=0;
                for(College c:nu.getCollege()){
                        for(Department d:c.getDepartments()){
                            for(Teacher t: d.getTeacherCatalog().getTeachers()){
                                sumArticle=t.getPublishedArticles()+sumArticle;
                                teachernumber++;
                            }
                        }
              } 
                averageArticle=sumArticle/teachernumber;
                System.out.println(nu.getUniversityName()+" : "+averageArticle);
        }
    }
}

