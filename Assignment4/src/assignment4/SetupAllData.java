/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment4;

import University.CalenderYear;
import University.ClassRoom;
import University.College;
import University.Course;
import University.CourseLoad;
import University.CourseOffering;
import University.Date;
import University.Degree;
import University.Department;
import University.Seat;
import University.SeatAssignment;
import University.Semester;
import University.Student;
import University.Teacher;
import University.University;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

/**
 *
 * @author Cuishaowen
 */
public class SetupAllData {
    public static void setAll(ArrayList<University> u){
        
        //read department name .xls file
        String[] departmentName = new String[27];
        int deptCount = 0;
        try{
    		 
             InputStream is = new FileInputStream("departmentlist.xls");
             jxl.Workbook rwb = Workbook.getWorkbook(is);
             Sheet rs = rwb.getSheet(0);
             
             Cell cell=rs.getCell(0, 0);
             int i=0,j=0;
             String b="";
             
             do
             {
            	 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String No=cell.getContents();
                 departmentName[i] = No;
                 
                 i++;
                 
                // cell=rs.getCell(0, i);
                // b=cell.getContents();
                 
             }while(i < 27);
             
             rwb.close();
         }
         catch(Exception e){
             e.printStackTrace();
         }
        
        //System.out.println(departmentName[0]);
        //System.out.println(departmentName[50]);
        //System.out.println(departmentName[79]);
        
        //read studen info .xls file
        ArrayList<Student> allStudent = new ArrayList<Student>();
        int studentCount = 0;
        try{
    		 
             InputStream is = new FileInputStream("student.xls");
             jxl.Workbook rwb = Workbook.getWorkbook(is);
             Sheet rs = rwb.getSheet(0);
             
             Cell cell=rs.getCell(0, 0);
             int i=0,j=0;
             String b="";
             
             do
             {
                 j = 0;
                 Student s1 = new Student();
                 
            	 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String firstName = cell.getContents();
                 s1.setFirstName(firstName);
                 s1.getStudentAccount().setUserName(firstName);
                 j++;
                 
                 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String lastName = cell.getContents();
                 s1.setLastName(lastName);
                 s1.getStudentAccount().setPwd(lastName);
                 j++;
                 
                 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String gender = cell.getContents();
                 s1.setGender(gender);
                 j++;
                 
                 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String dobYear = cell.getContents();
                 s1.setDobYear(dobYear);
                 j++;
                 
                 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String dobMonth = cell.getContents();
                 s1.setDobMonth(dobMonth);
                 j++;
                 
                 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String dobDay = cell.getContents();
                 s1.setDobDay(dobDay);
                 j++;
                 
                 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String studentID = cell.getContents();
                 s1.getStudentAccount().setStudentID(studentID);
                 j++;
                 
                 cell = rs.getCell(j, i);
                 String studentEmployment = cell.getContents();
                 int stet = Integer.parseInt(studentEmployment);
                 boolean whetherEmployed = false;
                 if (stet == 1)
                 {
                     whetherEmployed = true;
                 }
                 s1.setEmployeed(whetherEmployed);
                 
                 allStudent.add(s1);
                 
                 i++;
                 
                // cell=rs.getCell(0, i);
                // b=cell.getContents();
                 
             }while(i < 131);
             
             rwb.close();
         }
         catch(Exception e){
             e.printStackTrace();
         }
        
        ArrayList<Teacher> allTeacher = new ArrayList<Teacher>();
        int teacherCount = 0;
        try{
    		 
             InputStream is = new FileInputStream("teacher.xls");
             jxl.Workbook rwb = Workbook.getWorkbook(is);
             Sheet rs = rwb.getSheet(0);
             
             Cell cell=rs.getCell(0, 0);
             int i=0,j=0;
             String b="";
             
             do
             {
                 j = 0;
                 Teacher t1 = new Teacher();
                 
            	 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String firstName = cell.getContents();
                 t1.setFirstName(firstName);
                 j++;
                 
                 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String lastName = cell.getContents();
                 t1.setLastName(lastName);
                 j++;
                 
                 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String gender = cell.getContents();
                 t1.setGender(gender);
                 j++;
                 
                 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String dobYear = cell.getContents();
                 t1.setDobYear(dobYear);
                 j++;
                 
                 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String dobMonth = cell.getContents();
                 t1.setDobMonth(dobMonth);
                 j++;
                 
                 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String dobDay = cell.getContents();
                 t1.setDobDay(dobDay);
                 j++;
                 
                 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String teacherID = cell.getContents();
                 t1.setTeacherID(teacherID);
                 j++;
                 
                 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String publishedArticles = cell.getContents();
                 t1.setPublishedArticles(Integer.parseInt(publishedArticles));
                 j++;
                 
                 allTeacher.add(t1);
                 
                 i++;
                 
                 //cell=rs.getCell(0, i);
                // b=cell.getContents();
                 
             }while(i < 39);
             
             rwb.close();
         }
         catch(Exception e){
             e.printStackTrace();
         }
        
        ArrayList<Course> allCourse = new ArrayList<Course>();
        int coursesCount = 0;
        try{
    		 
             InputStream is = new FileInputStream("course.xls");
             jxl.Workbook rwb = Workbook.getWorkbook(is);
             Sheet rs = rwb.getSheet(0);
             
             Cell cell=rs.getCell(0, 0);
             int i=0,j=0;
             String b="";
             
             do
             {
                 j = 0;
                 
                 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String Name=cell.getContents();
                 j++;
                 
                 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String Num=cell.getContents();
                 j++;
                 
                 Course course = new Course(Name, Num);
                 
                 cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                 String core=cell.getContents();
                 
                 if(core.trim().equals("Core"))
                 {
                     course.setCoreCourse(true);
                 }
                 else
                 {
                     course.setCoreCourse(false);
                 }
                 
                 allCourse.add(course);
                 
                 i++;
                 
                 //cell=rs.getCell(0, i);
                // b=cell.getContents();
                 
             }while(i < 243);
             
             rwb.close();
         }
         catch(Exception e){
             e.printStackTrace();
         }
        
        for(University uni : u)
        {
            String[] buildingName = new String[4];
            if(uni.getUniversityName().equals("Northeastern University"))
            {
                buildingName[0] = "Beividere Place";
                buildingName[1] = "Cahners Hall";
                buildingName[2] = "Cargill Hall";
                buildingName[3] = "Dana Research Hall";
            }
            else if(uni.getUniversityName().equals("Boston University"))
            {
                buildingName[0] = "Churchill Hall";
                buildingName[1] = "Curry Hall";
                buildingName[2] = "Dockser Hall";
                buildingName[3] = "Cabot Hall";
            }
            else
            {
                buildingName[0] = "Speare Hall";
                buildingName[1] = "Smith Hall";
                buildingName[2] = "Egan Hall";
                buildingName[3] = "Columbus Hall";
            }
            
            for(College c : uni.getCollege())
            {
                for(int m = 0 ; m < 3 ; m++)
                {
                    if (deptCount > 80)
                    {
                        deptCount = 0;
                    }
                    Department dept = new Department(departmentName[deptCount]);
                    deptCount++;
                    c.addDepartments(dept);
                }
                for(Department d : c.getDepartments())
                {
                    if(c.getCollegeName().equals("Science"))
                    {
                        Degree degree = new Degree("PhD");
                        d.addDegree(degree);
                    }
                    else
                    {
                        Degree degree = new Degree("Certificate");
                        d.addDegree(degree);
                    }
                    
                    for(Degree degree : d.getDegree())
                    {
                        int cc = 0;
                        int ccc = 0;
                        for(Course c1 : allCourse)
                        {   
                            cc = 0;
                            if (coursesCount > allCourse.size())
                            {
                                coursesCount = 0;
                            }
                            if(ccc == coursesCount)
                            {
                                d.getDcc().addCourse(c1);
                                if(c1.isCoreCourse())
                                {
                                    degree.addCoreCourses(c1);
                                }
                                else
                                {
                                    degree.addElectives(c1);
                                }
                                cc++;
                                coursesCount++;
                            }
                            ccc++;
                            if(cc>2)
                            {
                                break;
                            }
                        }
                    }
                    
                    int k = 0;
                    for(int tt = 0 ; tt < 3 ; tt ++)
                    {
                        for(Teacher t2 : allTeacher)
                        {
                            if(teacherCount > allTeacher.size())
                            {
                                teacherCount = 0;
                            }
                            if(k == teacherCount)
                            {
                                d.getTeacherCatalog().addTeacher(t2);
                                
                                teacherCount++;
                                break;
                            }
                            k++;
                        }
                    }
                    
                    CalenderYear cy1 = new CalenderYear();
                    Date d1 = new Date();
                    d1.setYear("2014");
                    d1.setMonth("01");
                    d1.setDay("01");
                    Date d2 = new Date();
                    d2.setYear("2014");
                    d2.setMonth("12");
                    d2.setDay("31");
                    cy1.setFirstDate(d1);
                    cy1.setEndDate(d2);
                    Date d3 = new Date();
                    d3.setYear("2014");
                    d3.setMonth("01");
                    d3.setDay("10");
                    Date d4 = new Date();
                    d4.setYear("2014");
                    d4.setMonth("06");
                    d4.setDay("10");
                    Semester semester1 = new Semester(cy1, d4, d3);
                    d.getDcs().addSemesterCourseOffering(semester1);
                    
                    Date d7 = new Date();
                    d3.setYear("2014");
                    d3.setMonth("09");
                    d3.setDay("07");
                    Date d8 = new Date();
                    d4.setYear("2014");
                    d4.setMonth("12");
                    d4.setDay("17");
                    Semester semester2 = new Semester(cy1, d8, d7);
                    d.getDcs().addSemesterCourseOffering(semester2);
                    
                    CalenderYear cy2 = new CalenderYear();
                    Date d5 = new Date();
                    d5.setYear("2015");
                    d5.setMonth("01");
                    d5.setDay("01");
                    Date d6 = new Date();
                    d6.setYear("2015");
                    d6.setMonth("12");
                    d6.setDay("31");
                    cy2.setFirstDate(d1);
                    cy2.setEndDate(d2);
                    Date d9 = new Date();
                    d9.setYear("2014");
                    d9.setMonth("01");
                    d9.setDay("10");
                    Date d10 = new Date();
                    d10.setYear("2014");
                    d10.setMonth("06");
                    d10.setDay("10");
                    Semester semester3 = new Semester(cy2, d10, d9);
                    d.getDcs().addSemesterCourseOffering(semester3);
                    
                    Date d11 = new Date();
                    d11.setYear("2014");
                    d11.setMonth("09");
                    d11.setDay("07");
                    Date d12 = new Date();
                    d12.setYear("2014");
                    d12.setMonth("12");
                    d12.setDay("17");
                    Semester semester4 = new Semester(cy2, d12, d11);
                    d.getDcs().addSemesterCourseOffering(semester4);
                    
                    Semester[] sem = new Semester[4];
                    sem[0] = semester4;
                    sem[1] = semester3;
                    sem[2] = semester2;
                    sem[3] = semester1;
                    
                    String[] roomNum = new String[20];
                    roomNum[0] = "101";
                    roomNum[1] = "102";
                    roomNum[2] = "103";
                    roomNum[3] = "104";
                    roomNum[4] = "201";
                    roomNum[5] = "202";
                    roomNum[6] = "203";
                    roomNum[7] = "204";
                    roomNum[8] = "301";
                    roomNum[9] = "302";
                    roomNum[10] = "303";
                    roomNum[11] = "304";
                    roomNum[12] = "401";
                    roomNum[13] = "402";
                    roomNum[14] = "403";
                    roomNum[15] = "404";
                    roomNum[16] = "501";
                    roomNum[17] = "502";
                    roomNum[18] = "503";
                    roomNum[19] = "504";
                    int rNum = 0;
                    int building = 0;
                    int aa = 1;
                    int tcount = 1;
                    for(Semester se : d.getDcs().getSemesterCourseOffering())
                    {
                        for(Degree degree : d.getDegree())
                        {
                            int courseCount = 1;
                            for(Course course : degree.getCoreCourses())
                            {
                                if(courseCount == aa)
                                {
                                    int teachCount = 1;
                                    for(Teacher teacher : d.getTeacherCatalog().getTeachers())
                                    {
                                        if(teachCount == tcount)
                                        {
                                            if(rNum > 19)
                                            {
                                                rNum = 0;
                                            }
                                            if(building > 3)
                                            {
                                                building = 0;
                                            }
                                            ClassRoom cr = new ClassRoom(roomNum[rNum], buildingName[building]);
                                            rNum++;
                                            building++;
                                            
                                            CourseOffering co = new CourseOffering(cr, teacher, course);
                                            for(int j = 0 ;j <20 ; j++)
                                            {
                                                Seat seat = co.addSeat(String.valueOf(j));
                                            }
                                            se.addCourseOffering(co);
                                        }
                                        teachCount++;
                                    }
                                }
                                courseCount++;
                            }
                            
                        }
                        aa++;
                    }
                    
                    
                    for(int i = 0 ; i < 2 ; i++)
                    {
                        int kk = 0;
                        for(Student s2 : allStudent)
                        {
                            if(studentCount > allStudent.size())
                            {
                                studentCount = 0;
                            }
                            if(kk == studentCount)
                            {
                                int random = (int)(2+Math.random()*(4-2+1));
                                for(int a = 0 ; a < random ; a++)
                                {
                                    CourseLoad c1 = new CourseLoad();
                                    s2.getTranscript().addCourseLoad(c1);
                                }
                                int countSemester = 0;
                                for(CourseLoad cl : s2.getTranscript().getCourseLoad())
                                {
                                    cl.setSemester(sem[countSemester]);
                                    
                                    int ran = (int)(2+Math.random()*(4-2+1));
                                    for(int n = 0 ; n < ran ; n++)
                                    {
                                        SeatAssignment sassign = new SeatAssignment();
                                        cl.addSeatAssignment(sassign);
                                    }
                                    
                                    for(SeatAssignment sa : cl.getSeatAssignment())
                                    {
                                        float r = (float)(2+Math.random()*(4-2+1));
                                        sa.setGPA(r);
                                        sa.setCost(5688);
                                        
                                        for(Semester seme : d.getDcs().getSemesterCourseOffering())
                                        {
                                            if(seme.getStartDate() == cl.getSemester().getStartDate())
                                            {
                                                int r2 = (int)(2+Math.random()*(4-2+1));
                                                int seatAssign = 0;
                                                for(CourseOffering coff : seme.getCourseOffering())
                                                {
                                                    int r3 = (int)(1+Math.random()*(20-1+1));
                                                    for(Seat seat : coff.getSeat())
                                                    {
                                                        if(seat.getName().equals(String.valueOf(r3)))
                                                        {
                                                            seat.setAssigned(true);
                                                            sa.setSeat(seat);
                                                        }
                                                    }
                                                    seatAssign++;
                                                    if(seatAssign >= r2)
                                                    {
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    countSemester++;
                                }
                                
                                float rsalary = (float)(8+Math.random()*(12-8+1));
                                //System.out.println(rsalary);
                                s2.setSalary(rsalary);
                                
                                d.getDsd().addDepartmentStudentDirectory(s2);
                                c.getCsd().addStudent(s2);
                                uni.getUsd().addStudent(s2);
                                studentCount++;
                                break;
                            }
                            kk++;
                        }
                    }
                    
                    
                        
                    
                    
                    
                    
                    
                    
                }
            }
        }
    }
}
