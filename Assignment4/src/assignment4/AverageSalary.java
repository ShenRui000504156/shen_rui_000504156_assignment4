/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment4;

import University.College;
import University.Student;
import University.University;
import java.util.ArrayList;

/**
 *
 * @author fjj1213
 */
public class AverageSalary {
    public static void departmentProfessorArticles(ArrayList<University> u){
        float averageEngineerSalary=0;
        float sumEngineerSalary=0;
        float averageBusinessSalary=0;
        float sumBusinessSalary=0;
        int studentnumber=0;
        for(University un:u){
                            
            for(College c: un.getCollege()){
                if(c.getCollegeName().equals("Engineering")){
                            
                    for(Student s:c.getCsd().getCollegeStudentDirectory()){
                        sumEngineerSalary=s.getSalary()+sumEngineerSalary;
                        if(s.getSalary() != 0)
                        {
                            studentnumber++;
                        }
                    }
                }
                if(c.getCollegeName().equals("Business")){ 
                    for(Student s:c.getCsd().getCollegeStudentDirectory()){
                        sumBusinessSalary=s.getSalary()+sumBusinessSalary;
                        if(s.getSalary() != 0)
                        {
                            studentnumber++;
                        }
                    }
             
                }
                
            
            }
        }
        averageEngineerSalary=sumEngineerSalary/studentnumber;
        System.out.println("Engineer : "+averageEngineerSalary);
        averageBusinessSalary=sumBusinessSalary/studentnumber;
        System.out.println("Business : "+averageBusinessSalary);
    }
    
}
