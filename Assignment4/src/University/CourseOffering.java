/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;
import java.util.ArrayList;


/**
 *
 * @author fjj1213
 */
public class CourseOffering {
    private ClassRoom classRoom;
    private Teacher teacher;
    private ArrayList<Seat> seat;
    private Course course;

    public CourseOffering(ClassRoom cr, Teacher t, Course c) {
        seat= new ArrayList<Seat>();
        classRoom = cr;
        teacher = t;
        course = c;
    }

    public ClassRoom getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(ClassRoom classRoom) {
        this.classRoom = classRoom;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public ArrayList<Seat> getSeat() {
        return seat;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
    
    public Seat addSeat(String seatName){
        Seat s = new Seat(this, seatName);
        seat.add(s);
        return s;
    } 
    public void removeSeat(Seat s){
        seat.remove(s);
    }    
}
