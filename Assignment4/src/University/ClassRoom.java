/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;

/**
 *
 * @author fjj1213
 */
public class ClassRoom {
    
    private String roomNumber;
    private String building;

    public ClassRoom(String roomNumber, String building) {
        this.roomNumber = roomNumber;
        this.building = building;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }
}
