/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;

import java.util.ArrayList;

/**
 *
 * @author Rui
 */
public class CourseLoad {
    
    private Semester semester;
    private ArrayList<SeatAssignment> seatAssignment;
    
    public CourseLoad(){
        seatAssignment = new ArrayList<SeatAssignment>();
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }


    public ArrayList<SeatAssignment> getSeatAssignment() {
        return seatAssignment;
    }

    public void addSeatAssignment(SeatAssignment sa) {
        seatAssignment.add(sa);
    }
    
    
}
