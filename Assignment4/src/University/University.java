/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;

import java.util.ArrayList;

/**
 *
 * @author Cuishaowen
 */
public class University {
    private UniversityStudentDirectory usd;
    private ArrayList<College> college;
    
    private String universityName;

    public University(String uName) {
        college = new ArrayList<College>();
        College c1 = new College("Business");
        college.add(c1);
        College c2 = new College("Science");
        college.add(c2);
        College c3 = new College("Engineering");
        college.add(c3);
        
        usd = new UniversityStudentDirectory();
        universityName = uName;
    }

    public UniversityStudentDirectory getUsd() {
        return usd;
    }

    public void setUsd(UniversityStudentDirectory usd) {
        this.usd = usd;
    }

    public ArrayList<College> getCollege() {
        return college;
    }

    public void setCollege(ArrayList<College> college) {
        this.college = college;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }
    
    public void addCollege(College c){
        college.add(c);
    }
}
