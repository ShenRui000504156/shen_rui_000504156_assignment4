/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;

import java.util.ArrayList;

/**
 *
 * @author Rui
 */
public class Student extends Person{
    
  
    private StudentAccount studentAccount;
    private Transcript transcript;
    private boolean active;
    private boolean graduated;
    private boolean employeed;
    private int graduatedYear;
    private float salary;

    public Student(){
        studentAccount = new StudentAccount();
        active = true;
        graduated = true;
        employeed = false;
        transcript = new Transcript();
        salary = 0;
        graduatedYear = 2015;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public int getGraduatedYear() {
        return graduatedYear;
    }

    public void setGraduatedYear(int graduatedYear) {
        this.graduatedYear = graduatedYear;
    }
    
    public StudentAccount getStudentAccount() {
        return studentAccount;
    }

    public void setStudentAccount(StudentAccount studentAccount) {
        this.studentAccount = studentAccount;
    }

    public Transcript getTranscript() {
        return transcript;
    }

    public void setTranscript(Transcript transcript) {
        this.transcript = transcript;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isGraduated() {
        return graduated;
    }

    public void setGraduated(boolean graduated) {
        this.graduated = graduated;
    }

    public boolean isEmployeed() {
        return employeed;
    }

    public void setEmployeed(boolean employeed) {
        this.employeed = employeed;
    }
  
    
    
}
