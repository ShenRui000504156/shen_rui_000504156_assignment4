/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;

/**
 *
 * @author fjj1213
 */
public class Teacher extends Person {
    private String teacherID;
    private JobPosition jobposition;
    private FacultyRole facultyRole;
    private int publishedArticles;

    public int getPublishedArticles() {
        return publishedArticles;
    }

    public void setPublishedArticles(int publishedArticles) {
        this.publishedArticles = publishedArticles;
    }

    public String getTeacherID() {
        return teacherID;
    }

    public void setTeacherID(String teacherID) {
        this.teacherID = teacherID;
    }

    public JobPosition getJobposition() {
        return jobposition;
    }

    public void setJobposition(JobPosition jobposition) {
        this.jobposition = jobposition;
    }

    public FacultyRole getFacultyRole() {
        return facultyRole;
    }

    public void setFacultyRole(FacultyRole facultyRole) {
        this.facultyRole = facultyRole;
    }
}
