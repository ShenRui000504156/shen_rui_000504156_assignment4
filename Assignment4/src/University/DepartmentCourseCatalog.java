/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;

import java.util.ArrayList;

/**
 *
 * @author Cuishaowen
 */
public class DepartmentCourseCatalog {
    private ArrayList<Course> course;

    public DepartmentCourseCatalog() {
        course = new ArrayList<Course>();
    }

    public ArrayList<Course> getCourse() {
        return course;
    }

    public void addCourse(Course c) {
        course.add(c);
    }
    
    
}
