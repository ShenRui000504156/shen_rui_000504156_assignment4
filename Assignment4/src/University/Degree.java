/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;

import java.util.ArrayList;

/**
 *
 * @author Cuishaowen
 */
public class Degree {
    private String degreeName;
    
    private ArrayList<Course> coreCourses;
    private ArrayList<Course> electives;

    public Degree(String dName) {
        coreCourses = new ArrayList<Course>();
        electives = new ArrayList<Course>();
        
        degreeName = dName;
    }

    public String getDegreeName() {
        return degreeName;
    }

    public ArrayList<Course> getCoreCourses() {
        return coreCourses;
    }

    public void addCoreCourses(Course coreCourses) {
        this.coreCourses.add(coreCourses);
    }

    public ArrayList<Course> getElectives() {
        return electives;
    }

    public void addElectives(Course electives) {
        this.electives.add(electives);
    }
    
    
}
