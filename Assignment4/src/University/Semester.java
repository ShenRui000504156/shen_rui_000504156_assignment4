/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;

import java.util.ArrayList;

/**
 *
 * @author Cuishaowen
 */
public class Semester {
    private CalenderYear calenderYear;
    private Date endDate;
    private Date startDate;
    private ArrayList<CourseOffering> courseOffering;

    public Semester(CalenderYear cy, Date eDate, Date sDate) {
        courseOffering = new ArrayList<CourseOffering>();
        calenderYear = cy;
        endDate = eDate;
        startDate = sDate;
    }

    public CalenderYear getCalenderYear() {
        return calenderYear;
    }

    public void setCalenderYear(CalenderYear calenderYear) {
        this.calenderYear = calenderYear;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public ArrayList<CourseOffering> getCourseOffering() {
        return courseOffering;
    }

    public void addCourseOffering(CourseOffering courseOffering) {
        this.courseOffering.add(courseOffering);
    }
    
}
