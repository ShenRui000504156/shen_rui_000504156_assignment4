/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;

import java.util.ArrayList;

/**
 *
 * @author Cuishaowen
 */
public class TeacherCatalog {
    private ArrayList<Teacher> teachers;

    public TeacherCatalog() {
        teachers = new ArrayList<Teacher>();
    }

    public ArrayList<Teacher> getTeachers() {
        return teachers;
    }
    
    public void addTeacher(Teacher t){
        teachers.add(t);
    }
    
}
