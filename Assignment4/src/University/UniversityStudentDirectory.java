/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;

import java.util.ArrayList;

/**
 *
 * @author Cuishaowen
 */
public class UniversityStudentDirectory {
    private ArrayList<Student> students;

    public UniversityStudentDirectory() {
        students = new ArrayList<Student>();
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }
    
    public void addStudent(Student s){
        //Student s = new Student();
        students.add(s);
        //return s;
    }
}
