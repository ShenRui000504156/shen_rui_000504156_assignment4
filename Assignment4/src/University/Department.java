/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;

import java.util.ArrayList;

/**
 *
 * @author Cuishaowen
 */
public class Department {
    private ArrayList<JobPosition> jobPosition;
    private DepartmentCourseCatalog dcc;
    private ArrayList<Degree> degree;
    private DepartmentStudentDirectory dsd;
    private DepartmentCourseSchedule dcs;
    private TeacherCatalog teacherCatalog;

    private String departmentName;

    public Department(String dName) {
        jobPosition = new ArrayList<JobPosition>();
        
        FacultyRole fr1 = new FacultyRole("R-0001");
        JobPosition j1 = new JobPosition("Office Assistant");
        j1.setRole(fr1);
        jobPosition.add(j1);
        
        StaffRole sr1 = new StaffRole("R-0002");
        JobPosition j2 = new JobPosition("Athletics");
        j2.setRole(sr1);
        jobPosition.add(j2);
        
        FacultyRole fr2 = new FacultyRole("R-0003");
        JobPosition j3 = new JobPosition("Teacher");
        j3.setRole(fr2);
        jobPosition.add(j3);
        
        FacultyRole fr3 = new FacultyRole("R-0004");
        JobPosition j4 = new JobPosition("Teaching Assistant");
        j4.setRole(fr3);
        jobPosition.add(j4);
        
        FacultyRole fr4 = new FacultyRole("R-0005");
        JobPosition j5 = new JobPosition("Science Tutor");
        j5.setRole(fr4);
        jobPosition.add(j5);
        
        degree = new ArrayList<Degree>();
        Degree d1 = new Degree("Bachelor of Science");
        degree.add(d1);
        Degree d2 = new Degree("Master of Science");
        degree.add(d2);
        
        dcs = new DepartmentCourseSchedule();
        
        dcc = new DepartmentCourseCatalog();
        dsd = new DepartmentStudentDirectory();
        teacherCatalog = new TeacherCatalog();
        
        departmentName = dName;
    }

    public DepartmentCourseSchedule getDcs() {
        return dcs;
    }

    public void setDcs(DepartmentCourseSchedule dcs) {
        this.dcs = dcs;
    }
    
    public TeacherCatalog getTeacherCatalog() {
        return teacherCatalog;
    }

    public void setTeacherCatalog(TeacherCatalog teacherCatalog) {
        this.teacherCatalog = teacherCatalog;
    }

    public ArrayList<JobPosition> getJobPosition() {
        return jobPosition;
    }

    public void addJobPosition(JobPosition jp) {
        jobPosition.add(jp);
    }

    public DepartmentCourseCatalog getDcc() {
        return dcc;
    }

    public void setDcc(DepartmentCourseCatalog dcc) {
        this.dcc = dcc;
    }

    public ArrayList<Degree> getDegree() {
        return degree;
    }

    public void addDegree(Degree d) {
        degree.add(d);
    }

    public DepartmentStudentDirectory getDsd() {
        return dsd;
    }

    public void setDsd(DepartmentStudentDirectory dsd) {
        this.dsd = dsd;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
    
    
}
