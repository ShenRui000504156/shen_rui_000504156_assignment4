/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;

import java.util.ArrayList;

/**
 *
 * @author Rui
 */
public class DepartmentStudentDirectory {
    private ArrayList<Student> DepartmentStudentDirectory;

    public DepartmentStudentDirectory() {
        DepartmentStudentDirectory = new ArrayList<Student>();
    }

    public ArrayList<Student> getDepartmentStudentDirectory() {
        return DepartmentStudentDirectory;
    }

    public void addDepartmentStudentDirectory(Student s) {
        DepartmentStudentDirectory.add(s);
    }


    public void disableStudent(String studentID){
        for (Student student : DepartmentStudentDirectory){
            if (student.getStudentAccount().getStudentID().equals(studentID)){
                student.setActive(false);
            }
        }
    }
    
    
}
