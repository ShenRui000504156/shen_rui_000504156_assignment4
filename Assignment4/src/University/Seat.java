/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;

/**
 *
 * @author Rui
 */
public class Seat {
    private String name;
    private boolean assigned;
    private CourseOffering c;
    
    public Seat(CourseOffering c, String name){
        this.name = name;
        this.c = c;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAssigned() {
        return assigned;
    }

    public void setAssigned(boolean assigned) {
        this.assigned = assigned;
    }

    public CourseOffering getC() {
        return c;
    }

    public void setC(CourseOffering c) {
        this.c = c;
    }
    
    
    
}
