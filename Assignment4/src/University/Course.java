/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;

/**
 *
 * @author Cuishaowen
 */
public class Course {
    private Course preRequiste;
    private Course followp;
    
    private String courseName;
    private String courseNum;
    
    private int credithours;
    private float price;
    private boolean coreCourse;

    public Course(String cName, String cNum) {
        courseName = cName;
        courseNum = cNum;
        
        
    }

    public boolean isCoreCourse() {
        return coreCourse;
    }

    public void setCoreCourse(boolean coreCourse) {
        this.coreCourse = coreCourse;
    }

    public Course getPreRequiste() {
        return preRequiste;
    }

    public void setPreRequiste(Course preRequiste) {
        this.preRequiste = preRequiste;
    }

    public Course getFollowp() {
        return followp;
    }

    public void setFollowp(Course followp) {
        this.followp = followp;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseNum() {
        return courseNum;
    }

    public void setCourseNum(String courseNum) {
        this.courseNum = courseNum;
    }

    public int getCredithours() {
        return credithours;
    }

    public void setCredithours(int credithours) {
        this.credithours = credithours;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }


    
}
