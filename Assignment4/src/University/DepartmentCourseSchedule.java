/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;

import java.util.ArrayList;

/**
 *
 * @author Cuishaowen
 */
public class DepartmentCourseSchedule {
    private ArrayList<Semester> semesterCourseOffering;

    public DepartmentCourseSchedule() {
        semesterCourseOffering = new ArrayList<Semester>();
    }

    public ArrayList<Semester> getSemesterCourseOffering() {
        return semesterCourseOffering;
    }

    public void addSemesterCourseOffering(Semester semesterCourseOffering) {
        this.semesterCourseOffering.add(semesterCourseOffering);
    }
    
}
