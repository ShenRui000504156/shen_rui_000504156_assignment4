/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;

import java.util.ArrayList;

/**
 *
 * @author Rui
 */
public class CollegeStudentDirectory {
    private ArrayList<Student> CollegeStudentDirectory;
    
    public CollegeStudentDirectory(){
        CollegeStudentDirectory = new ArrayList<Student>();
    }

    public ArrayList<Student> getCollegeStudentDirectory() {
        return CollegeStudentDirectory;
    }

    public void setCollegeStudentDirectory(ArrayList<Student> CollegeStudentDirectory) {
        this.CollegeStudentDirectory = CollegeStudentDirectory;
    }
    
    public void addStudent(Student student){
        CollegeStudentDirectory.add(student);
    }
    
    public void disableStudent(String studentID){
        for (Student student : CollegeStudentDirectory){
            if (student.getStudentAccount().getStudentID().equals(studentID)){
                student.setActive(false);
            }
        }
    }
}
