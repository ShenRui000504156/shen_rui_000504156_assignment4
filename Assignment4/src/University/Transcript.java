/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;

import java.util.ArrayList;

/**
 *
 * @author Rui
 */
public class Transcript {
    private ArrayList<CourseLoad> courseLoad;
    
    public Transcript(){
        courseLoad = new ArrayList<CourseLoad>();
    }

    public ArrayList<CourseLoad> getCourseLoad() {
        return courseLoad;
    }

    public void addCourseLoad(CourseLoad cl) {
        courseLoad.add(cl);
    }
    
    
}
