/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package University;

import java.util.ArrayList;

/**
 *
 * @author Cuishaowen
 */
public class College {
    
    private ArrayList<Department> departments;
    private CollegeStudentDirectory csd;
    
    private String collegeName;

    public College(String cName) {
        departments = new ArrayList<Department>();
        collegeName = cName;
        csd = new CollegeStudentDirectory();
    }

    public ArrayList<Department> getDepartments() {
        return departments;
    }

    public void addDepartments(Department department) {
        departments.add(department);
    }

    public CollegeStudentDirectory getCsd() {
        return csd;
    }

    public void setCsd(CollegeStudentDirectory csd) {
        this.csd = csd;
    }

    public String getCollegeName() {
        return collegeName;
    }

    
}
