/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Framingham;

import java.util.ArrayList;

/**
 *
 * @author Cuishaowen
 */
public class Community {
    private ArrayList<House> houses;
    private CommunityPersonDirectory cpd;
    private int communityID;
    
    private static int cID;

    public Community() {
        houses = new ArrayList<House>();
        cpd = new CommunityPersonDirectory();
        cID++;
        communityID = cID;
    }

    public ArrayList<House> getHouses() {
        return houses;
    }

    public int getCommunityID() {
        return communityID;
    }

    public CommunityPersonDirectory getCpd() {
        return cpd;
    }
    
    public void addHouse(House h){
        houses.add(h);
    }
}
