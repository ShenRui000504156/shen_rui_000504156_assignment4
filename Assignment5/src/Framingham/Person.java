/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Framingham;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Cuishaowen
 */
public class Person {
    private Person mother;
    private Person father;
    private ArrayList<Person> siblings;
    
    private Patient patient;
    
    private static int identity;
    private boolean inFamily;
    
    private int id;
    private String name;
    private char gender;
    private int age;

    private int geneticScore;
    
    public Person() {
        identity++;
        id = identity;
        siblings = new ArrayList<Person>();
        inFamily = false;
    }

    public boolean isInFamily() {
        return inFamily;
    }

    public void setInFamily(boolean inFamily) {
        this.inFamily = inFamily;
    }

    public Person getMother() {
        return mother;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }

    public Person getFather() {
        return father;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    public ArrayList<Person> getSiblings() {
        return siblings;
    }

    public void addSiblings(Person sibling) {
        siblings.add(sibling);
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
     
    
}
