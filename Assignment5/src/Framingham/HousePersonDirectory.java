/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Framingham;

import java.util.ArrayList;

/**
 *
 * @author Cuishaowen
 */
public class HousePersonDirectory {
    private ArrayList<Person> persons;

    public HousePersonDirectory() {
        persons = new ArrayList<Person>();
    }
    
    public ArrayList<Person> getPerson() {
        return persons;
    }
    
    public void addPerson(Person p){
        persons.add(p);
    }
}
