/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Framingham;

import java.util.ArrayList;

/**
 *
 * @author Cuishaowen
 */
public class City {
    private ArrayList<Community> communities;
    private CityPersonDirectory cpd;

    public City() {
        communities = new ArrayList<Community>();
        cpd = new CityPersonDirectory();
    }

    public ArrayList<Community> getCommunities() {
        return communities;
    }
    
    public Community addCommunity(){
        Community c = new Community();
        communities.add(c);
        return c;
    }

    public CityPersonDirectory getCpd() {
        return cpd;
    }
}
