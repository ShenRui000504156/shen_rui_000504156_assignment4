/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Framingham;

import java.util.ArrayList;

/**
 *
 * @author Cuishaowen
 */
public class House {
    private ArrayList<Family> families;
    private HousePersonDirectory hpd;
    private int houseID;
    
    private static int hID;

    public House() {
        families = new ArrayList<Family>();
        hpd = new HousePersonDirectory();
        hID++;
        houseID = hID;
    }

    public ArrayList<Family> getFamilies() {
        return families;
    }

    public HousePersonDirectory getHpd() {
        return hpd;
    }

    public int getHouseID() {
        return houseID;
    }
    
    public void addFamily(Family f){
        families.add(f);
    }
}
