/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment5;

import Framingham.City;
import Framingham.Community;
import Framingham.Family;
import Framingham.House;
import Framingham.Person;
import Framingham.VitalSign;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

/**
 *
 * @author Cuishaowen
 */
public class SetUpAllData {
    public static void SetUpAll(City city){
        
        //read person infomation .xls file
        int[][] relationship = new int[1000][];
        try{ 
            InputStream is = new FileInputStream("departmentlist.xls");
            jxl.Workbook rwb = Workbook.getWorkbook(is);
            Sheet rs = rwb.getSheet(0);
             
            Cell cell=rs.getCell(0, 0);
            int i=0,j=0;
            String b="";
             
            do
            {
                Person p = new Person();
                 
            	cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                String id=cell.getContents();
                j++;
                 
                cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                String name=cell.getContents();
                p.setName(name);
                j++;
                
//                cell = rs.getCell(j, i);//第一位是列号，第二位是行号
//                String year=cell.getContents();
//                p.setBirthYear(year);
//                j++;
                
                cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                String gender=cell.getContents();
                p.setGender(gender.charAt(0));
                j++;
                
                cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                String father=cell.getContents();
                relationship[i][0] = Integer.parseInt(father);
                j++;
                
                cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                String mother=cell.getContents();
                relationship[i][1] = Integer.parseInt(mother);
                j++;
                
                cell = rs.getCell(j, i);
                b = cell.getContents();
                
                int sibling = 2;
                while(b.trim().equals("null"))
                {
                    relationship[i][sibling] = Integer.parseInt(b);
                    sibling++;
                    if(sibling > 6)
                    {
                        break;
                    }
                    j++;
                    cell = rs.getCell(j, i);
                    b = cell.getContents();
                }
                
                i++;
                 
                // cell=rs.getCell(0, i);
                // b=cell.getContents();
                 
            }while(i < 1000);
             
            rwb.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
        ArrayList<Family> families = new ArrayList<Family>();
        
        //set one person's family information
        int pCount = 0;
        for(Person p : city.getCpd().getPersons())
        {
            int i = 0;
            p.setFather(city.getCpd().getPersons().get(relationship[pCount][i]));
            i++;
            p.setMother(city.getCpd().getPersons().get(relationship[pCount][i]));
            i++;
            for(int j = i ; j < relationship[pCount].length ; j++)
            {
                p.addSiblings(city.getCpd().getPersons().get(relationship[pCount][i]));
                city.getCpd().getPersons().get(relationship[pCount][i]).setInFamily(true);
            }
            
            //set family
            if(p.isInFamily() == false)
            {
                Family f = new Family();
                f.addPerson(p);
                p.setInFamily(true);
                f.addPerson(p.getFather());
                f.addPerson(p.getMother());
                for(Person sib : p.getSiblings())
                {
                    f.addPerson(sib);
                }
                families.add(f);
            }
        }
        
        //set house
        ArrayList<House> houses = new ArrayList<House>();
        House h1 = new House();
        houses.add(h1);
        houses.get(houses.size() - 1).addFamily(families.get(0));
        for(int i = 0 ; i < families.size() ; i++)
        {
            boolean f = false;
            for(Person pi : families.get(i).getPersons())
            {
                for(Person pj : families.get(i+1).getPersons())
                {
                    if(pi == pj)
                    {
                        f = true;
                    }
                }
            }
            if(f == true)
            {
                houses.get(houses.size() - 1).addFamily(families.get(i+1));
                break;
            }
            else
            {
                House h = new House();
                houses.add(h);
                houses.get(houses.size() - 1).addFamily(families.get(i+1));
                break;
            }
        }
        
        for(House h : houses)
        {
            for(Family f : h.getFamilies())
            {
                for(Person p : f.getPersons())
                {
                    for(int i = 0 ; i < h.getHpd().getPerson().size() ; i++)
                    {
                        if(p == h.getHpd().getPerson().get(i))
                        {
                            continue;
                        }
                        else
                        {
                            h.getHpd().addPerson(p);
                        }
                    }
                }
            }
        }
        
        //set community
        for(int ii = 0 ; ii < 10 ; ii ++)
        {
            boolean f = false;
            Community community = city.addCommunity();
            for(int j = 0 ; j < 8 ; j++)
            {
                community.addHouse(houses.get(ii*8+j));
                community.getCpd().getPersons().addAll(houses.get(ii*8+j).getHpd().getPerson());
                if(ii*8+j+1 > houses.size())
                {
                    f = true;
                    break;
                }
            }
            if(f == true)
            {
                break;
            }
        }
        
        //set score information
        for(Person p : city.getCpd().getPersons())
        {
            Random rand = new Random();
            
            //set every person's vital sign
            for(int i = 0 ; i < 10 ; i++)
            {
                VitalSign vs = p.getPatient().addVitalSigns();
                
                int randomSBP = rand.nextInt((200-110)+1)+110;
                vs.setSbp(randomSBP);
                int randomDBP = rand.nextInt((110-50)+1)+50;
                vs.setDbp(randomDBP);
                double randomTC = rand.nextDouble() * 9 + 1;
                vs.setTotalCholesterol(randomTC);
                double randomHDLC = rand.nextDouble() * 2;
                vs.setHdlCholesterol(randomHDLC);
                
                vs.getDate().setYear(2016);
                vs.getDate().setMonth(10);
                vs.getDate().setDay(15 + i);
                int randomHour = rand.nextInt((23-0)+1);
                vs.getDate().setHour(randomHour);
                int randomMinite = rand.nextInt((59-0)+1);
                vs.getDate().setMinite(randomMinite);
            }
            
            //set smoker
            double randomSmoker = rand.nextDouble();
            if(randomSmoker > 0.5)
            {
                p.getPatient().setSmoker(true);
            }
            else
            {
                p.getPatient().setSmoker(false);
            }
            
            //set diabetes
            double randomDiabetes = rand.nextDouble();
            if(randomDiabetes > 0.5)
            {
                p.getPatient().setDiabetes(true);
            }
            else
            {
                p.getPatient().setDiabetes(false);
            }
            
            //set age
            if(p.getFather() == null && p.getMother() == null)
            {
                continue;
            }
            else if(p.getFather().getFather() == null && p.getFather().getMother() == null && p.getMother().getFather() == null && p.getMother().getMother() == null) 
            {
                continue;
            }
            else
            {
                int randomAge = rand.nextInt((45-20)+1)+20;
                p.setAge(randomAge);
                if(p.getFather() != null && p.getFather().getAge() != 0)
                {
                    p.getFather().setAge(randomAge + 30);
                }
                if(p.getMother() != null && p.getMother().getAge() != 0)
                {
                    p.getMother().setAge(randomAge + 30);
                }
                if(p.getFather().getFather() != null && p.getFather().getFather().getAge() != 0)
                {
                    p.getFather().getFather().setAge(randomAge + 60);
                }
                if(p.getFather().getMother() != null && p.getFather().getMother().getAge() != 0)
                {
                    p.getFather().getMother().setAge(randomAge + 60);
                }
                if(p.getMother().getFather() != null && p.getMother().getFather().getAge() != 0)
                {
                    p.getMother().getFather().setAge(randomAge + 60);
                }
                if(p.getMother().getMother() != null && p.getMother().getMother().getAge() != 0)
                {
                    p.getMother().getMother().setAge(randomAge + 60);
                }
            }
        }
        
        
    }
}
